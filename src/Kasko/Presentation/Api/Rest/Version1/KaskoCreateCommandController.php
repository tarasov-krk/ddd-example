<?php

declare(strict_types=1);

namespace App\Kasko\Presentation\Api\Rest\Version1;

use App\Kasko\Application\Command\CreateDraftHandler;

#[Doc\Tag('kasko')]
#[Route('/kasko')]
#[Doc\Response(response: 400, description: 'Wrong request parameters', content: new Doc\JsonContent(ref: '#/components/schemas/error'))]
final class KaskoCreateCommandController extends AbstractController
{
    #[Route('/drafts/create', methods: ['POST'])]
    public function createDraftAction(Request $request, CreateDraftHandler $createDraftHandler): JsonResponse
    {
        $author = $request->getUserFromRequest();
        $draft = $createDraftHandler->execute($author);

        return $this->json([
            'uuid' => $draft->getUuid()
        ]);
    }
}

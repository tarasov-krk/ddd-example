<?php

namespace App\Kasko\Infrastructure\Persistence\Repository;


use App\Kasko\Domain\Aggregate\KaskoPolicy;
use App\Kasko\Domain\Repository\KaskoPolicyReadStorage;
use App\Kasko\Domain\Repository\KaskoPolicyWriteStorage;
use App\Kasko\Domain\Repository\UuidInterface;
use App\Kasko\Infrastructure\Persistence\Enum\KaskoStatusEnum;

class KaskoPolicyRepository extends ServiceEntityRepository implements KaskoPolicyWriteStorage, KaskoPolicyReadStorage
{
    public function getByUuid(UuidInterface $uuid): KaskoPolicy
    {
        return $this->find($uuid);
    }

    public function create(KaskoPolicy $kaskoPolicy): KaskoPolicy
    {
        $kaskoPolicyDoctrine = \App\Kasko\Infrastructure\Persistence\ORM\KaskoPolicy::create(
            match ($kaskoPolicy->getStatus()) {
                \App\Kasko\Domain\Aggregate\Enum\KaskoStatusEnum::CREATED => KaskoStatusEnum::CREATED,
                // ...
            },
            $kaskoPolicy->getAuthor()->getId(),
        );

        $this->_em->persist($kaskoPolicyDoctrine);

        // $kaskoPolicyDoctrine => $kaskoPolicy
        $kaskoPolicy->setUuid(
            $kaskoPolicyDoctrine->getUuid()
        );
        $kaskoPolicy->setStatus(
            match ($kaskoPolicyDoctrine->getStatus()) {
                KaskoStatusEnum::CREATED => \App\Kasko\Domain\Aggregate\Enum\KaskoStatusEnum::CREATED,
                // ...
            }
        );

        return $kaskoPolicy;
    }
}

<?php

namespace App\Kasko\Infrastructure\Persistence\ORM;

use \Ramsey\UuidInterface;
//use App\Kasko\Domain\Repository\UuidInterface;
use App\Kasko\Infrastructure\Persistence\Enum\KaskoStatusEnum;
use App\Kasko\Infrastructure\Persistence\Repository\KaskoPolicyRepository;
use App\Shared\Infrastructure\Trait\Timestampable;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: KaskoPolicyRepository::class)]
#[ORM\Table(name: '`kasko__policy`')]
#[ORM\HasLifecycleCallbacks]
class KaskoPolicy
{
    use Timestampable;

    #[ORM\Column]
    private UuidInterface $uuid;

    #[ORM\Column]
    private KaskoStatusEnum $status;

    #[ORM\Column]
    private int $authorId;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
    }

    public static function create(
        KaskoStatusEnum $statusEnum,
        int $authorId
//        \App\Kasko\Domain\Aggregate\KaskoPolicy $kaskoPolicy
    ): self {
        $self = new self;

        $self->status = $statusEnum;
        $self->authorId = $authorId;

        return $self;
    }

    /**
     * @return UuidInterface
     */
    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    /**
     * @return KaskoStatusEnum
     */
    public function getStatus(): KaskoStatusEnum
    {
        return $this->status;
    }
}

<?php

namespace App\Kasko\Infrastructure\Persistence\Enum;

enum KaskoStatusEnum: string
{
    case CREATED = 'created';
    case CALCULATED = 'calculated';
    case PAID = 'paid';
}

<?php

namespace App\Kasko\Domain\Aggregate\Enum;

enum KaskoStatusEnum: string
{
    case CREATED = 'created';
    case CALCULATED = 'calculated';
    case PAID = 'paid';
}

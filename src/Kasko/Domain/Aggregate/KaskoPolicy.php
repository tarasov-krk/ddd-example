<?php

namespace App\Kasko\Domain\Aggregate;

use App\Kasko\Domain\Repository\KaskoUserStorageInterface;
use App\Kasko\Domain\Repository\UuidInterface;
use App\Kasko\Domain\Aggregate\Enum\KaskoStatusEnum;

class KaskoPolicy
{
    private ?UuidInterface $uuid;
    private KaskoStatusEnum $status;
    private KaskoUserStorageInterface $author;

    public function setUuid(UuidInterface $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    public function setStatus(KaskoStatusEnum $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getStatus(): KaskoStatusEnum
    {
        return $this->status;
    }

    public function getAuthor(): KaskoUserStorageInterface
    {
        return $this->author;
    }

    public function setAuthor(KaskoUserStorageInterface $author): KaskoPolicy
    {
        $this->author = $author;

        return $this;
    }
}

<?php

declare(strict_types=1);

namespace App\Kasko\Domain\Service\KaskoPolicy;

use App\Kasko\Domain\Aggregate\Enum\KaskoStatusEnum;
use App\Kasko\Domain\Aggregate\KaskoPolicy;
use App\Kasko\Domain\Repository\KaskoPolicyWriteStorage;
use App\Kasko\Domain\Repository\KaskoUserStorageInterface;

final readonly class KaskoPolicyCreator implements KaskoPolicyCreatorInterface
{
    public function __construct(
        private KaskoPolicyWriteStorage $kaskoPolicyWriteStorage,
    ) {}

    public function create(
        KaskoUserStorageInterface $author,
        KaskoStatusEnum $status,
    ): KaskoPolicy {
        $kaskoPolicy = new KaskoPolicy();

        $kaskoPolicy->setStatus($status);
        $kaskoPolicy->setAuthor($author);

        return $this->kaskoPolicyWriteStorage->create($kaskoPolicy);
    }
}

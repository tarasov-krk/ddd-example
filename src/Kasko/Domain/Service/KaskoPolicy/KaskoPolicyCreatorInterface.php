<?php

declare(strict_types=1);

namespace App\Kasko\Domain\Service\KaskoPolicy;

use App\Kasko\Domain\Aggregate\Enum\KaskoStatusEnum;
use App\Kasko\Domain\Aggregate\KaskoPolicy;
use App\Kasko\Domain\Repository\KaskoUserStorageInterface;

interface KaskoPolicyCreatorInterface
{
    public function create(
        KaskoUserStorageInterface $author,
        KaskoStatusEnum $status,
    ): KaskoPolicy;
}

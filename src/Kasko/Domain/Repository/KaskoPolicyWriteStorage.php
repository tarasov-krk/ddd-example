<?php

namespace App\Kasko\Domain\Repository;

use App\Kasko\Domain\Aggregate\KaskoPolicy;

interface KaskoPolicyWriteStorage
{
    public function create(KaskoPolicy $kaskoPolicy): KaskoPolicy;
}

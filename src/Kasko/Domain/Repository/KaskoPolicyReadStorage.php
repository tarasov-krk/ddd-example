<?php

namespace App\Kasko\Domain\Repository;

use App\Kasko\Domain\Aggregate\KaskoPolicy;

interface KaskoPolicyReadStorage
{
    public function getByUuid(UuidInterface $uuid): KaskoPolicy;
}

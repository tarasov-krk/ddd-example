<?php

namespace App\Kasko\Application\Command;

use App\Kasko\Domain\Aggregate\Enum\KaskoStatusEnum;
use App\Kasko\Domain\Aggregate\KaskoPolicy;
use App\Kasko\Domain\Service\KaskoPolicy\KaskoPolicyCreatorInterface;
use App\Kasko\Infrastructure\Persistence\Repository\KaskoUserStorage;
use App\Shared\Domain\Repository\UserStorageInterface;

final readonly class CreateDraftHandler extends AbstractCommand
{
    public function __construct(
        private KaskoPolicyCreatorInterface $kaskoPolicyCreator,
    ) {}

    public function execute(UserStorageInterface $user): KaskoPolicy
    {
        $kaskoUser = new KaskoUserStorage();
        $kaskoUser->setId($user->getId());

        return $this->kaskoPolicyCreator->create(
            $kaskoUser,
            KaskoStatusEnum::CREATED
        );
    }
}

<?php

namespace App\Shared\Domain\Repository;

interface UserStorageInterface
{
    public function getId(): int;
    public function setId(int $id): void;
}

<?php

namespace App\Shared\Application\Dto;

final readonly class UserDto
{
    public function __construct(
        public string $username,
        public string $email,
    ) {
    }
}

<?php

namespace App\Shared\Infrastructure\Repository;

use App\Shared\Domain\Repository\UserStorageInterface;

final class UserStorage implements UserStorageInterface
{
    private int $id;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }
}
